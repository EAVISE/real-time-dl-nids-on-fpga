import numpy as np
import torch
from torch.utils.data import DataLoader

from learning.prerequisites import get_net, get_dataset
from evaluation.evaluation import Evaluation


def unsupervised_inference(experiment, data_arrays, output_path, verbose=True):
    pass


def sanity_check(experiment, input_sample, verbose=True):
    """
    Verify the functioning of the model by calculating the output for one input sample.

    :param experiment: Experiment that lead to the trained model to verify.
    :param input_sample: Numpy array to serve as input sample.
    :param verbose: If True, provide verbose output.

    :return: The output of the model for the provided input sample
    """

    print(input_sample)
    # Define device:
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Load trained model:
    neural_net = get_net(experiment)
    neural_net.load_state_dict(torch.load(experiment.best_model_path))

    # Call eval() and send the net to the training device
    neural_net.eval()
    neural_net.to(device)

    with torch.no_grad():
        inputs = torch.from_numpy(input_sample).to(device)
        output = neural_net(inputs.float())

    input_npy = inputs.float().cpu().numpy()
    output_npy = output.cpu().numpy()

    print(input_npy, output_npy)

    return input_npy, output_npy


def supervised_inference(experiment, data_arrays, evaluation_arrays, output_path, verbose=True):

    # Set input data to the correct type:
    target_dtype = experiment.dataset_interface.get_dtype()

    if target_dtype != data_arrays.dtype:
        data_arrays = data_arrays.astype(target_dtype)

    # Define device:
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Load trained model:
    neural_net = get_net(experiment)
    neural_net.load_state_dict(torch.load(experiment.best_model_path))

    # Call eval() and send the net to the training device
    neural_net.eval()
    neural_net.to(device)

    predictions = []

    n_samples = data_arrays.shape[0]
    with torch.no_grad():
        for i in range(n_samples):

            # Input tensor to device
            data_sample = data_arrays[i]
            data_sample = np.expand_dims(np.expand_dims(data_sample, axis=0), axis=0)
            inputs = torch.from_numpy(data_sample).to(device)

            # Calculate output
            #print(inputs.float())
            outputs = neural_net(inputs.float())

            #print(outputs)
            _, predicted = torch.max(outputs, 1)

            predictions += predicted

            if verbose:
                print('{} / {}'.format(i, n_samples), end='\r')

    predictions = torch.stack(predictions, 0).cpu().numpy()
    print(predictions)
    evaluation = Evaluation(evaluation_arrays, predictions, labels=experiment.get_labels(), frac=False)
    eval_text = evaluation.classification_report(output_dict=False)

    with open(output_path, 'w') as f:
        f.write(",".join([str(x) for x in predictions]) + '\n')
        f.write(",".join([str(x) for x in evaluation_arrays]) + '\n')
        f.write('\nEvaluation report\n')
        f.write(eval_text + '\n')
        f.write(evaluation.evaluate_ids(experiment.dataset_interface.get_normal_class()))