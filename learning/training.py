import time
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from sklearn import metrics

from learning.prerequisites import get_net, get_dataset, get_optimizer


def _validation_loop(dataloader, neural_net, optimizer, criterion, epoch, device, valid_visual_batches,
                     scheduler=None, experiment=None, same_epoch=False):
    predictions = []
    actual = []
    running_loss = 0.0
    total_loss = 0.0
    print('\nValidation:')
    for i, data in enumerate(dataloader, 0):
        # Inference
        inputs = data["item"].to(device)
        labels = data["label"].to(device)
        outputs = neural_net(inputs.float())

        # Calculate metrics
        loss = criterion(outputs, labels)
        _, predicted = torch.max(outputs, 1)
        predictions += predicted
        actual += labels

        running_loss += loss.item()
        total_loss += loss.item()
        if i % valid_visual_batches == (valid_visual_batches - 1):  # print every 2000 mini-batches
            # print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / valid_visual_batches), end='\r')
            visualize_progress(epoch, i, running_loss, valid_visual_batches, len(dataloader), end='\r')
            running_loss = 0.0

    actual = torch.stack(actual, 0).cpu().numpy()
    predictions = torch.stack(predictions, 0).cpu().numpy()

    accuracy = metrics.accuracy_score(actual, predictions, normalize=True) * 100
    avg_loss = total_loss / len(dataloader)

    print()
    print('Accuracy: {:.2f}%'.format(accuracy))
    print('Avg loss: {:.4f}'.format(avg_loss))

    # If applicable, update learning rate
    if not (scheduler is None):
        scheduler.step(avg_loss)

    # If applicable, update the training status
    if not (experiment is None):
        experiment.update_train_status(same_epoch=same_epoch,
                                       accuracy=accuracy, avg_loss=avg_loss, lr=optimizer.param_groups[0]['lr'])

    # If applicable, save the current network weights
    if experiment.continue_tr:
        # Save current progress
        torch.save(neural_net.state_dict(), experiment.model_path)
        experiment.save()

    # If applicable, save the current best results
    if avg_loss < experiment.best_loss:
        torch.save(neural_net.state_dict(), experiment.best_model_path)
        experiment.best_loss = avg_loss
        experiment.save()

    # torch.cuda.empty_cache()
    return predictions, actual, avg_loss


def _inside_training_loop(data, neural_net, optimizer, criterion, device):
    #print('Data samples: {}'.format(data["item"][0][:10]))
    #print('Data samples shape: {}'.format(data["item"].shape))

    t1 = time.time()
    # get the inputs; data is a list of [inputs, labels]
    inputs = data["item"].to(device)
    # decode_tensor(data["item"], FeatureStrategy[experiment.features_config.features_config.featurestrategy])
    # inputs.to(device)
    labels = data["label"].to(device)
    # labels.to(device)

    # zero the parameter gradients
    optimizer.zero_grad()

    # forward + backward + optimize
    # call .float() for this reason:
    # https://github.com/pytorch/pytorch/issues/2138
    outputs = neural_net(inputs.float())
    # CrossEntropyLoss takes an index as input rather than a one-hot encoded vector!
    # https://discuss.pytorch.org/t/runtimeerror-multi-target-not-supported-newbie/10216/2
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()
    t2 = time.time()

    return loss.item(), t2 - t1


def train_experiment(experiment, verbose=True):
    """
    Execute the training of an experiment.

    :param experiment: Experiment to train
    :param verbose: If True, print additional informative text as output
    :type experiment: ``MLExperiment``
    :type verbose: ``bool``
    :return: None
    """
    # Define device:
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # device = torch.device("cpu")
    if verbose:
        if torch.cuda.is_available():
            print('Using GPU: {}'.format(device))
        else:
            print('Using CPU')

    # Define CNN:
    neural_net = get_net(experiment)

    if experiment.continue_tr and experiment.is_continued():
        neural_net.load_state_dict(torch.load(experiment.model_path))

    neural_net.to(device)

    # Get Dataset objects
    if verbose:
        print("Loading dataset memory maps")
    trainset = get_dataset(experiment=experiment, usage='train')
    validset = get_dataset(experiment=experiment, usage='valid')

    # Create dataloaders
    if verbose:
        print("Generating dataloaders")

    batch_size = experiment.batch_size
    trainloader = DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=experiment.num_workers)
    validloader = DataLoader(validset, batch_size=batch_size, shuffle=True, num_workers=experiment.num_workers)

    # Update verbose output every 1% progress
    train_visual_batches = len(trainloader) // 100
    valid_visual_batches = len(validloader) // 100

    # Set loss strategy:
    criterion = nn.CrossEntropyLoss()

    # Select optimizer
    if verbose:
        print('Using optimizer:', experiment.optimizer)

    print('LR is')
    print(experiment.get_current_lr(), type(experiment.get_current_lr()))
    print()
    optimizer = get_optimizer(experiment.optimizer.name)(neural_net.parameters(), lr=float(experiment.get_current_lr()),
                                                         **experiment.optimizer.get_parameters())

    # Select LR scheduler
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
                                                     factor=experiment.factor,
                                                     patience=experiment.patience,
                                                     verbose=verbose,
                                                     threshold=experiment.threshold,
                                                     cooldown=experiment.cooldown)

    train_steps_per_validation = experiment.train_steps_per_validation

    stopcondition = StoppingCondition(experiment=experiment,
                                      condition=experiment.stopping_condition,
                                      current_epoch=experiment.get_current_epoch(),
                                      current_lowest=experiment.get_current_average_loss(),
                                      patience=experiment.patience,
                                      max_epochs=experiment.n_epochs)

    # Print global information before starting the training
    if verbose:
        print("Model:", experiment.model)
        print("Starting training for", experiment.get_remaining_epochs(), "epochs")
        print('Training for', len(experiment.get_labels()), 'output classes')
        print('Optimizer:', experiment.optimizer.name)
        print('Loss function: Cross-Entropy Loss')
        print('Mini-batch size:', batch_size)
        print('Training steps per epoch:', len(trainloader))
        print('Training steps per validation:', train_steps_per_validation)
        print('Validation steps:', len(validloader))

    # Training
    # for epoch in range(experiment.get_remaining_epochs()):  # loop over the dataset multiple times
    while not stopcondition:
        # Get the current epoch
        epoch = stopcondition.current_epoch

        # Training loop
        neural_net.train()
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            loss, tdif = _inside_training_loop(data=data, neural_net=neural_net, optimizer=optimizer,
                                               criterion=criterion, device=device)
            running_loss += loss
            # print statistics
            if i % train_visual_batches == (train_visual_batches - 1):  # print every train_visual_batches mini-batches
                visualize_progress(epoch, i, running_loss, train_visual_batches, len(trainloader),
                                   timedif=round(tdif * 1000), end='\r')
                running_loss = 0.0

            # Perform validation every train_steps_per_validation steps
            # + 1 to avoid 0 triggering a validation
            if (train_steps_per_validation > 1) and ((i + 1) % train_steps_per_validation == 0):
                with torch.no_grad():
                    neural_net.eval()
                    _validation_loop(validloader, neural_net, optimizer, criterion, epoch, device, valid_visual_batches,
                                     scheduler=scheduler, experiment=experiment, same_epoch=True)

                    neural_net.train()

        # Validation
        with torch.no_grad():
            neural_net.eval()
            predictions, actual, avg_loss = _validation_loop(validloader, neural_net, optimizer, criterion,
                                                             epoch, device, valid_visual_batches,
                                                             scheduler=scheduler, experiment=experiment)
            stopcondition.update(loss=avg_loss)
        # model_storage_name = baseline_name + "-{}.model".format(time.time())
    print('Finished training loop:')
    report = metrics.classification_report(actual, predictions, target_names=experiment.get_labels())
    print(report)
    experiment.status['train']['complete'] = True
    experiment.update()
    # Save the trained model
    torch.save(neural_net.state_dict(), experiment.model_path)
    print('Finished Training')


def visualize_progress(epoch, i, running_loss, visual_batches, n_batches=None, timedif=None, end='\n'):
    # Calculate progress:
    if n_batches is None:
        progress = i + 1
    else:
        progress = (i / n_batches) * 100

    # Baseline string:
    baseline = '[{:d}, {:.2f}%] loss: {:.3f} '.format(epoch + 1, progress, running_loss / visual_batches)

    if timedif is not None:
        baseline += 'Time/batch: {}'.format(timedif)

    print(baseline, end=end)


class StoppingCondition:

    def __init__(self, experiment, condition, current_epoch, **kwargs):
        self.condition = condition
        self.experiment = experiment
        self.current_epoch = current_epoch

        if self.condition == 'epochs':
            self.n_epochs = kwargs.get('n_epochs', 100)
        elif self.condition == 'loss_decrease':
            self.target = kwargs.get('target', 0.0)
            self.lowest_loss = kwargs.get('current_lowest', float('inf'))
            self.patience = kwargs.get('patience', 10)
            self.n_unchanged_epochs = 0
            self.max_epochs = kwargs.get('max_epochs', -1)

    def update(self, loss=None):
        self.current_epoch += 1

        if self.condition == 'loss_decrease':
            if loss < self.lowest_loss:
                self.lowest_loss = loss
            else:
                self.n_unchanged_epochs += 1

    def __bool__(self):
        if self.condition == 'epochs':
            return self.experiment.get_remaining_epochs() == 0
        elif self.condition == 'loss_decrease':
            return (self.n_unchanged_epochs >= self.patience) or (self.lowest_loss <= self.target) \
                   or ((self.max_epochs - 1) <= self.current_epoch)
        else:
            return True
