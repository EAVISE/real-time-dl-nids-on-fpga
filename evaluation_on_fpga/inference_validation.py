import numpy as np
import os
from finn.core.datatype import DataType
from driver_base import FINNExampleOverlay
from evaluation import Evaluation

# dictionary describing the I/O of the FINN-generated accelerator
io_shape_dict = {
    # FINN DataType for input and output tensors
    "idt" : DataType.UINT8,
    "odt" : DataType.INT16,
    # shapes for input and output tensors (NHWC layout)
    "ishape_normal" : (1, 22, 22, 1),
    "oshape_normal" : (1, 13),
    # folded / packed shapes below depend on idt/odt and input/output
    # PE/SIMD parallelization settings -- these are calculated by the
    # FINN compiler.
    "ishape_folded" : (1, 22, 22, 1, 1),
    "oshape_folded" : (1, 1, 13),
    "ishape_packed" : (1, 22, 22, 1, 1),
    "oshape_packed" : (1, 1, 26)
}


# allmax function source: https://stackoverflow.com/a/58652335
def allmax(a):
    """
    Returns the indices of each occurence of the max value in an array.

    :param a:
    :return:
    """
    if len(a) == 0:
        return []
    all_ = [0]
    max_ = a[0]
    for i in range(1, len(a)):
        if a[i] > max_:
            all_ = [i]
            max_ = a[i]
        elif a[i] == max_:
            all_.append(i)
    return all_


def analyze_results(actual, predictions, labels):
    ev = Evaluation(actual, predictions, labels, False)
    report = ev.classification_report_text()
    ds_is = ev.evaluate_ids(0)
    return report + '\n' + ds_is + '\n'
    #pass

if __name__ == '__main__':

    # parse arguments
    #args = parser.parse_args()
    #exec_mode = args.exec_mode
    platform = 'zynq-iodma' # Alternative: 'alveo'
    batch_size = 1
    bitfile = 'finn-accel.bit'
    #inputfile = args.inputfile
    #outputfile = args.outputfile
    runtime_weight_dir = "runtime_weights/"

    # instantiate FINN accelerator driver and pass batchsize and bitfile
    accel = FINNExampleOverlay(
        bitfile_name=bitfile, platform=platform,
        io_shape_dict=io_shape_dict, batch_size=batch_size,
        runtime_weight_dir=runtime_weight_dir
    )

    # load the data files:
    data_file = 'CICIDS2017_qbasecnn2d_true-data.npy'
    evaluation_file = 'CICIDS2017_qbasecnn2d_true-eval.npy'
    class_labels = ['Benign', 'Bot', 'PortScan', 'DDoS', 'Web Attack', 'Infiltration', 'DoS GoldenEye',
                    'DoS Hulk', 'DoS Slowhttptest', 'DoS Slowloris', 'Heartbleed', 'FTP-Patator', 'SSH-Patator']
    data_arrays = np.load(data_file, 'r')
    eval_arrays = np.load(evaluation_file, 'r')

    n_samples = data_arrays.shape[0]
    
    predictions = []
    actual = []

    for i in range(n_samples):
        data_sample = data_arrays[i]
        ibuf_normal = np.expand_dims(np.expand_dims(data_sample, axis=2), axis=0)
        obuf_normal = accel.execute(ibuf_normal)

        predicted_idx = allmax(obuf_normal[0])[0]
        actual_idx = eval_arrays[i]
        #print(predicted_idx, actual_idx)
        print('Prediction <-> Actual: {} <-> {}'.format(class_labels[predicted_idx], class_labels[actual_idx]))

        predictions.append(predicted_idx)
        actual.append(actual_idx)
        #print(obuf_normal[0], allmax(obuf_normal[0]), eval_arrays[i])

    print(analyze_results(actual, predictions, class_labels))
