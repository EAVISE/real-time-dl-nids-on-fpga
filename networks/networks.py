"""
Module for the definition of different neural network architectures.

Author: Laurens Le Jeune


References:
[1] W. Wang et al., “HAST-IDS: Learning Hierarchical Spatial-Temporal Features Using Deep Neural Networks to\
    Improve Intrusion Detection,” IEEE Access, vol. 6, pp. 1792–1806, 2018, doi: 10.1109/ACCESS.2017.2780250.

[2] Y. Zhang, X. Chen, D. Guo, M. Song, Y. Teng, and X. Wang, “PCCN: Parallel Cross Convolutional Neural Network for\
    Abnormal Network Traffic Flows Detection in Multi-Class Imbalanced Network Traffic Flows,”\
    IEEE Access, vol. 7, pp. 119904–119916, 2019, doi: 10.1109/access.2019.2933165.

[3] https://github.com/echowei/DeepTraffic/blob/master/3.HAST-IDS/iscx2012_cnn_rnn_5class.py

[4] https://github.com/chenxu93/imbalanced_flow

[5] S. Bai, J. Z. Kolter, and V. Koltun, “An empirical evaluation of generic convolutional and recurrent networks for\
    sequence modeling,”arXiv:1803.01271, 2018.

[6] https://github.com/locuslab/TCN
"""
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.nn.utils import weight_norm


def conv1d_output_size(input_size, filter_size, padding, stride):
    """
    Calculate the output size of a feature vector going through a 1D convolutional layer

    :param input_size: Size of the input feature vector
    :type input_size: ``int``
    :param filter_size: Size of the filter
    :type filter_size: ``int``
    :param padding: Number of padded zeros on both sides of the feature vector
    :type padding: ``int``
    :param stride:Stride of the filter
    :type stride: ``int``
    :return: Output size of the feature vector
    :rtype: ``int``
    """
    return int(np.floor((input_size - filter_size + 2 * padding) / stride) + 1)


class SimpleNet(nn.Module):
    """
    Source: https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html
    """
    def __init__(self, num_classes):
        super(SimpleNet, self).__init__()
        # 1 input image channel, 6 output channels, 3x3 square convolution
        # kernel
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(16 * 2 * 2, 54)  # 6*6 from image dimension
        self.fc2 = nn.Linear(54, 40)
        self.fc3 = nn.Linear(40, num_classes)

    def forward(self, x):
        # Max pooling over a (2, 2) window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        # If the size is a square you can only specify a single number
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


def from_string(classname):
    return getattr(sys.modules[__name__], classname)


class TiedSDAClassifier(nn.Module):
    def __init__(self, num_classes, shapes=None):
        super(TiedSDAClassifier, self).__init__()
        if shapes is None:
            shapes = [1000, 256, 100]

        self.sda = TiedStackedDenoisingAutoEncoder(shapes)
        self.out = nn.Linear(in_features=shapes[-1], out_features=num_classes)
        self.softmax = nn.Softmax()

    def get_AE(self, index):
        """
        Get the autoencoder with the specified index. Indices work as following:\
            x      x\
            x x  x x\
            x x  x x\
            x x  x x\
            x      x
        Idx 0 1  1 0

        :param index: Index of the autoencoder in the network
        :type index: ``int``
        :return:
        """
        return self.sda.get_AE(index)

    def forward(self, x):
        x = self.sda(x)
        x = self.softmax(self.out(x))

        return x


class TiedStackedDenoisingAutoEncoder(nn.Module):

    def __init__(self, shapes):
        super(TiedStackedDenoisingAutoEncoder, self).__init__()
        self.shapes = shapes    # eg. [1000, 256, 100, 10]
        self.layers = nn.ModuleList()

        # Generate each layer as an individual autoencoder
        for i in range(1, len(self.shapes)):
            self.layers.append(TiedDenoisingAutoEncoder(self.shapes[i - 1], self.shapes[i]))

    def get_AE(self, index):
        """
        Get the autoencoder with the specified index. Indices work as following:\
            x      x\
            x x  x x\
            x x  x x\
            x x  x x\
            x      x
        Idx 0 1  1 0

        :param index: Index of the autoencoder in the network
        :type index: ``int``
        :return:
        """
        return self.layers[index]

    def forward(self, x):

        for ae in self.layers:
            encoded, decoded = ae(x, corruption=0.0)
            x = encoded

        return x


class TiedDenoisingAutoEncoder(nn.Module):
    # Sources for implementation idea:
    # https://gist.github.com/InnovArul/500e0c57e88300651f8005f9bd0d12bc
    # https://discuss.pytorch.org/t/how-to-create-and-train-a-tied-autoencoder/2585/11
    def __init__(self, input_size, hidden_size):
        super(TiedDenoisingAutoEncoder, self).__init__()
        self.param = nn.Parameter(torch.randn(hidden_size, input_size), requires_grad=True)

    def forward(self, x, corruption=0.0):

        if self.training and (0.0 < corruption < 1.0):
            x = F.dropout(x, corruption)

        encoded = torch.sigmoid(F.linear(x, self.param))
        decoded = torch.sigmoid(F.linear(encoded, self.param.t()))
        return encoded, decoded


class HAST1IDS(nn.Module):
    """
    HAST-I IDS based on the description in [1].

    """
    def __init__(self, input_shape=(256, 600), kernel_sizes=(5, 5), output_channels=(32, 64), num_classes=5):
        super(HAST1IDS, self).__init__()
        # Padding is 'same' -> for 3x3 convolution kernels that requires zero-padding of 1

        self.conv1 = nn.Conv1d(in_channels=input_shape[0],
                               out_channels=output_channels[0],
                               kernel_size=kernel_sizes[0],
                               padding=kernel_sizes[0] // 2, stride=1)

        # We add padding to the feature maps before max pooling,
        # in order to ensure all 'pixels' are included
        # Note that, as we use max-pooling, we use pad with - infinity
        # Note that, as we pool with kernel of size and stride 3,
        # the total length of the feature map should be a multiple of 3
        pad = 3 - input_shape[1] % 3
        if pad == 3:
            pad = 0
        self.size_after_padding1 = (input_shape[1] + pad) / 3
        self.pad1 = nn.ConstantPad1d((0, pad), - float('inf'))

        self.pool1 = nn.MaxPool1d(kernel_size=3, stride=3)
        self.conv2 = nn.Conv1d(in_channels=output_channels[0],
                               out_channels=output_channels[1],
                               kernel_size=kernel_sizes[1],
                               padding=kernel_sizes[1] // 2, stride=1)
        pad = 3 - self.size_after_padding1 % 3
        if pad == 3:
            pad = 0
        self.size_after_padding2 = (self.size_after_padding1 + pad) / 3
        self.pad2 = nn.ConstantPad1d((0, pad), - float('inf'))
        self.pool2 = nn.MaxPool1d(kernel_size=3, stride=3)

        self.dense_in = self.size_after_padding2 * output_channels[1]
        self.dense_mid = 1024
        self.num_classes = num_classes
        self.dense1 = nn.Linear(in_features=self.dense_in, out_features=self.dense_mid)
        self.dense2 = nn.Linear(in_features=self.dense_mid, out_features=num_classes)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pad1(x)
        x = self.pool1(x)
        x = F.relu(self.conv2(x))
        x = self.pad2(x)
        x = self.pool2(x)
        x = x.view(-1, self.dense_in)
        x = F.relu(self.dense1(x))
        x = F.softmax(self.dense2(x))

        return x


class HAST2CNNBranch(nn.Module):
    """
    This object is a branch for the HAST-II IDS and consists out of a number of 1D convolution layers, as
    well as pooling layers to reduce dimensionality. It will take  (N, p, q) images as input,
    where N is the batch size, p is the dimensionality of an one-hot encoded vector (should be 256), and q is the
    number of bytes of a network packet that are one-hot encoded. Basically, the input is a batch of 1D images with
    a size of q and p channels, much like a regular color image is a m x n image with 3 color channels.
    The output of this branch is a (N, 256) vector.

    :param input_shape: Input shape of the 1D image DISREGARDING batch size. Default (p=256, q=100)
    :param kernel_sizes: Sizes of the kernels in the two 1D convolution layers.
    :param output_channels: Number of filters for each 1D convolution layer
    :type input_shape: ``tuple`` of [``int``, ``int``]
    :type kernel_sizes: ``tuple`` of [``int``, ``int``]
    :type output_channels: ``tuple`` of [``int``, ``int``]

    :return: 256 neurons for each (p, q) 1D image.
    :rtype: (N, 256) torch.tensor, where N is the batch size
    """
    def __init__(self, input_shape=(256, 100), kernel_sizes=(5, 5),
                 output_channels=(128, 256)):
        super(HAST2CNNBranch, self).__init__()
        # For 1D convolution layers: We assume data to be ordered in this manner: (N, Cin, Lin)
        # This will result in the output data of (N, Cout, Lout)
        # Here, N is the batch size, C is the number of channels and L is the size of a feature map
        # Therefore, for 100 x 256-OHE bytes, Cin = 256 and Lin = 100

        # As is the case in [2], the 1D convolution layers have 'valid' padding. In this case, that means
        # no zero-padding.
        self.conv1 = nn.Conv1d(in_channels=input_shape[0],
                               out_channels=output_channels[0],
                               kernel_size=kernel_sizes[0],
                               padding=0, stride=1)
        self.pool1 = nn.MaxPool1d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv1d(in_channels=output_channels[0],
                               out_channels=output_channels[1],
                               kernel_size=kernel_sizes[1],
                               padding=0, stride=1)
        self.pool2 = nn.MaxPool1d(kernel_size=2, stride=2)

        self.dense_in = output_channels[1]
        self.dense_out = 128
        self.dense = nn.Linear(in_features=self.dense_in, out_features=self.dense_out)

    def forward(self, x):
        x = torch.tanh(self.conv1(x))
        x = self.pool1(x)
        x = torch.tanh(self.conv2(x))
        x = self.pool2(x)
        # print(x.size(), x.size()[2:])
        # Global max pooling using a regular maxpooling layer with kernel size = input size
        x = F.max_pool1d(x, kernel_size=x.size()[2:])
        # print(x.size())
        # Use view to turn [N, 1, 256] vector into [N, 256] vector, as required for a linear layer (the dense layer)
        x = x.view(-1, self.dense_in)
        # print(x.size())
        x = F.relu(self.dense(x))
        # print(x.size())

        return x


class HASTParallelCNN(nn.Module):
    def __init__(self, input_shape=(256, 100), kernel_sizes=((5, 5), (7, 5)),
                 output_channels=((128, 256), (192, 320))):
        super(HASTParallelCNN, self).__init__()
        self.cnn1 = HAST2CNNBranch(input_shape, kernel_sizes[0], output_channels[0])
        self.cnn2 = HAST2CNNBranch(input_shape, kernel_sizes[1], output_channels[1])

    def forward(self, x):
        y1 = self.cnn1(x)
        y2 = self.cnn2(x)
        return torch.cat((y1, y2), 1)


class HAST2IDS(nn.Module):
    """
    HAST-II IDS as described in [1] and [3].

    """
    def __init__(self, input_shape=(6, 256, 100), kernel_sizes=((5, 5), (7, 5)),
                 output_channels=((128, 256), (192, 320)), batch_first=True,
                 num_classes=5):

        super(HAST2IDS, self).__init__()
        # 1 input image channel, 32 output channels, 3x3 square convolutionkernel
        # Note that dilation 1 is just normal convolution without dilation
        # Padding is 'same' -> for 3x3 convolution kernels that requires zero-padding of 1

        # Input shape: 14 packets of 600 bytes of 256-dimensional vectors: 14, 256, 600
        # First define the processing of a single network packet: 256, 600

        cnn = HASTParallelCNN(input_shape[-2:], kernel_sizes, output_channels)
        self.timedivision = TimeDistributed(cnn, batch_first=batch_first)
        self.drop1 = nn.Dropout(0.1)
        # self.lstm1 = nn.LSTM(input_size=256, hidden_size=92, dropout=0.1, batch_first=batch_first)
        self.lstm1 = nn.LSTM(input_size=256, hidden_size=92, batch_first=batch_first)
        self.drop2 = nn.Dropout(0.1)
        # self.lstm2 = nn.LSTM(input_size=92, hidden_size=92, dropout=0.1, batch_first=batch_first)
        self.lstm2 = nn.LSTM(input_size=92, hidden_size=92, batch_first=batch_first)
        self.dense = nn.Linear(in_features=92, out_features=num_classes)

    def forward(self, x):
        x = self.timedivision(x)
        x = self.drop1(x)
        x = torch.tanh(self.lstm1(x)[0])
        x = self.drop2(x)
        # We only want to use the output of the LSTM after the last timestep
        # So we use the final output or we use the final hidden state
        # LSTM returns output, (h, c) with h the hidden state
        #x = torch.tanh(self.lstm2(x)[0][-1])
        #out, h = self.lstm2(x)
        #x = out[:, -1, :]
        x = torch.tanh(self.lstm2(x)[0][:, -1, :])
        x = F.softmax(self.dense(x), dim=1)

        return x


class HAST2IDS2(nn.Module):
    """
    HAST-II IDS as described in [1] and [3].

    """
    def __init__(self, input_shape=(6, 256, 100), kernel_sizes=((5, 5), (7, 5)),
                 output_channels=((128, 256), (192, 320)), batch_first=True,
                 num_classes=5):

        super(HAST2IDS2, self).__init__()
        # 1 input image channel, 32 output channels, 3x3 square convolutionkernel
        # Note that dilation 1 is just normal convolution without dilation
        # Padding is 'same' -> for 3x3 convolution kernels that requires zero-padding of 1

        # Input shape: 14 packets of 600 bytes of 256-dimensional vectors: 14, 256, 600
        # First define the processing of a single network packet: 256, 600

        self.cnn1 = nn.Conv1d(in_channels=256, out_channels=128, stride=1, padding=2, kernel_size=20)
        self.pool1 = nn.MaxPool1d(kernel_size=2, stride=2)
        self.cnn2 = nn.Conv1d(in_channels=128, out_channels=256, stride=1, padding=1, kernel_size=10)
        self.pool2 = nn.MaxPool1d(kernel_size=2, stride=2)
        self.dense = nn.Linear(in_features=256, out_features=num_classes)

    def forward(self, x):
        #x = self.timedivision(x)
        x = x[:, 0]
        x = self.pool1(F.tanh(self.cnn1(x)))
        x = self.pool2(F.tanh(self.cnn2(x)))
        x = F.max_pool1d(x, kernel_size=x.size()[2:])
        x = x.view(-1, 256)
        x = self.dense(x)
        x = F.softmax(x, dim=0)
        #x = F.softmax(self.alternate_dense(x), dim=1)
        #x = self.drop1(x)
        #x = torch.tanh(self.lstm1(x)[0])
        #x = self.drop2(x)
        # We only want to use the output of the LSTM after the last timestep
        # So we use the final output or we use the final hidden state
        # LSTM returns output, (h, c) with h the hidden state
        #x = torch.tanh(self.lstm2(x)[0][-1])
        #out, h = self.lstm2(x)
        #x = out[:, -1, :]
        #x = torch.tanh(self.lstm2(x)[0][:, -1, :])
        #x = F.softmax(self.dense(x), dim=1)

        return x


class TimeDistributed(nn.Module):
    """
    Adapted from https://discuss.pytorch.org/t/any-pytorch-function-can-work-as-keras-timedistributed/1346/4
    """
    def __init__(self, module, batch_first=False):
        super(TimeDistributed, self).__init__()
        self.module = module
        self.batch_first = batch_first

    def forward(self, x):

        if len(x.size()) <= 2:
            return self.module(x)

        # Squash samples and timesteps into a single axis
        # x is (N, T, L) vector, where N is the batch size, T is the number timesteps and L is the
        # x_reshape = x.contiguous().view(-1, x.size(-1))  # (samples * timesteps, input_size)
        x_reshape = x.contiguous().view(-1, x.size(-2), x.size(-1))  # (samples * timesteps, one-hot-encoding, bytes)
        y = self.module(x_reshape)

        # We have to reshape Y
        if self.batch_first:
            # y = y.contiguous().view(x.size(0), -1, y.size(-1))  # (samples, timesteps, output_size)
            y = y.contiguous().view(x.size(0), -1, y.size(-1))  # (samples, timesteps, OHE, bytes)
        else:
            # y = y.view(-1, x.size(1), y.size(-1))  # (timesteps, samples, output_size)
            y = y.view(-1, x.size(1), y.size(-1))  # (timesteps, samples, OHE, bytes)

        return y


class BaseCNN2D(nn.Module):
    def __init__(self, num_classes=10, head_payload=False):
        super(BaseCNN2D, self).__init__()

        if head_payload:
            self.avg_kernel_size = 3
            #self.i_size = 22
        else:
            self.avg_kernel_size = 2
            #self.i_size = 16
        self.num_class = num_classes
        #self.input_size = None
        #self.input_size = (self.i_size, self.i_size, 1)
        #self.p = 1 if self.i_size == 22 else 0

        # n x 22 x 22
        self.layer_A_conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*16
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 22 x 22
        self.layer_A_pool1 = self.layer_A_pool1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 8*8*32
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 11 x 11
        self.layer_A_conv2 = nn.Sequential(
            nn.Conv2d(32, 96, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*96
            nn.BatchNorm2d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 11 x 11
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv2d(96, 256, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 4*4*256
            nn.BatchNorm2d(256, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 6 x 6
        self.avg_pool = nn.MaxPool2d(kernel_size=self.avg_kernel_size, stride=2, ceil_mode=False)  # 1*1*256

        # n x 2 x 2
        self.fc = nn.Sequential(
            nn.BatchNorm1d(2 * 2 * 256),
            nn.Dropout(0.5),
            nn.Linear(2 * 2 * 256, self.num_class, bias=True)
        )

    def __conv(self, x):
        x = self.layer_A_conv1(x)
        x = self.layer_A_pool1(x)
        x = self.layer_A_conv2(x)
        x = self.layer_A_pool2(x)

        x = self.avg_pool(x)

        return x

    def forward(self, x):
        x = self.__conv(x)
        x = x.view(x.shape[0], -1)
        x = self.fc(x)

        return x


class BaseCNN2D2(nn.Module):
    def __init__(self, num_classes=10, head_payload=False):
        super(BaseCNN2D2, self).__init__()

        if head_payload:
            self.avg_kernel_size = 3
            #self.i_size = 22
        else:
            self.avg_kernel_size = 2
            #self.i_size = 16
        self.num_class = num_classes
        #self.input_size = None
        #self.input_size = (self.i_size, self.i_size, 1)
        #self.p = 1 if self.i_size == 22 else 0

        # n x 22 x 22
        self.layer_A_conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*16
            nn.BatchNorm2d(16, eps=1e-05),
            nn.ReLU(),
        )

        # n x 22 x 22
        self.layer_A_pool1 = self.layer_A_pool1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 8*8*32
            nn.BatchNorm2d(32, eps=1e-05),
            nn.ReLU(),
        )

        # n x 11 x 11
        self.layer_A_conv2 = nn.Sequential(
            nn.Conv2d(32, 96, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*96
            nn.BatchNorm2d(96, eps=1e-05),
            nn.ReLU(),
        )

        # n x 11 x 11
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv2d(96, 256, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 4*4*256
            nn.BatchNorm2d(256, eps=1e-05),
            nn.ReLU(),
        )

        # n x 6 x 6
        #self.avg_pool = nn.MaxPool2d(kernel_size=self.avg_kernel_size, stride=2, ceil_mode=False)  # 1*1*256
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        # n x 2 x 2
        self.fc = nn.Sequential(
            #nn.BatchNorm1d(2 * 2 * 256),
            #nn.Dropout(0.5),
            nn.Linear(256, self.num_class, bias=True)
        )

    def __conv(self, x):
        x = self.layer_A_conv1(x)
        x = self.layer_A_pool1(x)
        x = self.layer_A_conv2(x)
        x = self.layer_A_pool2(x)

        x = self.avg_pool(x)

        return x

    def forward(self, x):
        x = self.__conv(x)
        x = x.view(x.shape[0], -1)
        x = self.fc(x)

        return x


class BaseCNN2dNP(nn.Module):
    def __init__(self, num_classes=10, in_channels=1):
        super(BaseCNN2dNP, self).__init__()

        self.num_class = num_classes

        # n x 22 x 22
        self.layer_A_conv1 = nn.Sequential(
            nn.Conv2d(in_channels, 16, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 22 x 22
        self.layer_A_pool1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 11 x 11

        self.layer_A_conv2 = nn.Sequential(
            nn.Conv2d(32, 96, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),
            nn.BatchNorm2d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # n x 11 x 11
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv2d(96, 128, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),
            nn.BatchNorm2d(128, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        # N X 6 X 6
        self.layer_A_pool3 = nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),
            nn.BatchNorm2d(256, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        # n x 3 x 3

        #self.avg_pool = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=False)  # 1*1*256

        self.fc = nn.Sequential(
            #nn.BatchNorm1d(2 * 2 * 256),
            #nn.Dropout(0.5),
            nn.Linear(3 * 3 * 256, self.num_class, bias=True)
        )

    def __conv(self, x):
        x = self.layer_A_conv1(x)
        #print(x.shape)
        x = self.layer_A_pool1(x)
        #print(x.shape)
        x = self.layer_A_conv2(x)
        #print(x.shape)
        x = self.layer_A_pool2(x)
        #print(x.shape)
        x = self.layer_A_pool3(x)
        #x = self.avg_pool(x)
        #print(x.shape)

        return x

    def forward(self, x):
        #print(x.shape)
        x = self.__conv(x)
        x = x.view(x.shape[0], -1)
        x = self.fc(x)

        return x


class BaseCNN1D(nn.Module):
    def __init__(self, num_classes=10, input_size=512):
        super(BaseCNN1D, self).__init__()

        self.num_class = num_classes
        self.input_size = input_size

        # Find input vector size of fully connected layers
        self.fc_size = self.input_size

        # We reduce size twice according to the network architecture
        self.fc_size = conv1d_output_size(self.fc_size, filter_size=9, padding=4, stride=4)
        self.fc_size = conv1d_output_size(self.fc_size, filter_size=9, padding=4, stride=4)
        # Account for the average pooling
        self.fc_size = int(np.ceil(self.fc_size / 4))

        self.layer1 = nn.Sequential(
            nn.Conv1d(1, 16, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # -> input_size x 16
            nn.BatchNorm1d(num_features=16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer2 = nn.Sequential(
            nn.Conv1d(16, 32, kernel_size=9, stride=4, dilation=1, padding=4, bias=True),  # -> (input_size / 4) x 32
            nn.BatchNorm1d(num_features=32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer3 = nn.Sequential(
            nn.Conv1d(32, 96, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # -> (input_size / 4) x 96
            nn.BatchNorm1d(num_features=96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer4 = nn.Sequential(
            nn.Conv1d(96, 256, kernel_size=9, stride=4, dilation=1, padding=4, bias=True),  # -> (input_size / 16) x 256
            nn.BatchNorm1d(num_features=256, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.avgpool = nn.AvgPool1d(kernel_size=4, stride=4, ceil_mode=True)

        self.classification = nn.Linear(in_features=self.fc_size * 256, out_features=self.num_class, bias=True)

    def __convolve(self, x):
        print(x.shape)
        x = self.layer1(x)
        print(x.shape)
        x = self.layer2(x)
        print(x.shape)
        x = self.layer3(x)
        print(x.shape)
        x = self.layer4(x)
        print(x.shape)
        return x

    def forward(self, x):
        # Process convolution operations
        x = self.__convolve(x)
        x = self.avgpool(x)
        print(x.shape)
        # Reshape the features to a 1D vector: n_batches x (input_size / 16 * 256)
        x = x.view(x.shape[0], -1)

        print(x.shape)

        # Perform classification
        x = self.classification(x)

        return x


class CNN1D(nn.Module):
    def __init__(self, num_classes=10, input_size=500):
        super(CNN1D, self).__init__()

        self.num_class = num_classes
        self.input_size = (input_size, 1)
        self.p = 1 if self.i_size == 22 else 0

        self.layer_A_conv1 = nn.Sequential(
            nn.Conv1d(1, 16, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # 500*16
            nn.BatchNorm1d(16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool1 = self.layer_A_pool1 = nn.Sequential(
            nn.Conv1d(16, 32, kernel_size=9, stride=2, dilation=1, padding=4, bias=True),  # 250*32
            nn.BatchNorm1d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_conv2 = nn.Sequential(
            nn.Conv1d(32, 64, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # 250*64
            nn.BatchNorm1d(64, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv1d(64, 96, kernel_size=9, stride=2, dilation=1, padding=4, bias=True),  # 125*256
            nn.BatchNorm1d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer_B_conv1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # 500*32
            nn.BatchNorm1d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool1 = nn.MaxPool1d(kernel_size=2, stride=2, padding=0, )  # 250*32
        self.layer_B_conv2 = nn.Sequential(
            nn.Conv1d(32, 64, kernel_size=9, stride=1, dilation=1, padding=4, bias=True),  # 250*64
            nn.BatchNorm1d(64, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool2 = nn.MaxPool1d(kernel_size=2, stride=2, padding=self.p, )  # 125*128

        self.global_conv = nn.Sequential(
            nn.Conv1d(128, 160, kernel_size=9, stride=2, dilation=1, padding=4, bias=True),  # 2*2*160
            nn.BatchNorm1d(160, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.point_conv1 = nn.Conv1d(64, 32, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)
        self.point_conv2 = nn.Conv1d(128, 64, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)
        self.point_conv3 = nn.Conv1d(160, 128, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)

        self.avg_pool = nn.AvgPool1d(kernel_size=self.avg_kernel_size, stride=2, ceil_mode=False)  # 1*1*160

        self.fc = nn.Sequential(
            nn.BatchNorm1d(1 * 1 * 160),
            nn.Dropout(0.5),
            nn.Linear(1 * 1 * 160, self.num_class, bias=True)
        )


class CROSS_CNN(nn.Module):
    """docstring for CROSS_CNN"""

    def __init__(self, num_classes=10, head_payload=False):
        super(CROSS_CNN, self).__init__()
        if head_payload:
            self.avg_kernel_size = 3
            self.i_size = 22
        else:
            self.avg_kernel_size = 2
            self.i_size = 16
        self.num_class = num_classes
        self.input_size = None
        self.input_size = (self.i_size, self.i_size, 1)
        self.p = 1 if self.i_size == 22 else 0

        self.layer_A_conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*16
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool1 = self.layer_A_pool1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 8*8*32
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_conv2 = nn.Sequential(
            nn.Conv2d(64, 96, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*96
            nn.BatchNorm2d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv2d(192, 256, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 4*4*256
            nn.BatchNorm2d(256, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer_B_conv1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*32
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool1 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0, )  # 8*8*32
        self.layer_B_conv2 = nn.Sequential(
            nn.Conv2d(64, 96, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*96
            nn.BatchNorm2d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool2 = nn.MaxPool2d(kernel_size=2, stride=2, padding=self.p, )  # 4*4*192

        self.global_conv = nn.Sequential(
            nn.Conv2d(448, 896, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 2*2*896
            nn.BatchNorm2d(896, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.avg_pool = nn.AvgPool2d(kernel_size=self.avg_kernel_size, stride=2, ceil_mode=False)  # 1*1*896

        self.fc = nn.Sequential(
            nn.BatchNorm1d(1 * 1 * 896),
            nn.Dropout(0.5),
            nn.Linear(1 * 1 * 896, self.num_class, bias=True)
        )

    def features(self, input_data):
        x_A_conv1 = self.layer_A_conv1(input_data)  # 16*16*16
        x_B_conv1 = self.layer_B_conv1(input_data)  # 16*16*32
        x_A_pool1 = self.layer_A_pool1(x_A_conv1)  # 16*16*32
        x_B_pool1 = self.layer_B_pool1(x_B_conv1)  # 8*8*32

        x_A_cat1 = torch.cat((x_A_pool1, x_B_pool1), 1)  # 8*8*64
        x_B_cat1 = torch.cat((x_B_pool1, x_A_pool1), 1)  # 8*8*64

        x_A_conv2 = self.layer_A_conv2(x_A_cat1)  # 8*8*96
        x_B_conv2 = self.layer_B_conv2(x_B_cat1)  # 8*8*96

        x_A_cat2 = torch.cat((x_A_conv2, x_B_conv2), 1)  # 8*8*192
        x_B_cat2 = torch.cat((x_B_conv2, x_A_conv2), 1)  # 8*8*192

        x_A_pool2 = self.layer_A_pool2(x_A_cat2)  # 4*4*256
        x_B_pool2 = self.layer_B_pool2(x_B_cat2)  # 4*4*192

        x_global_cat = torch.cat((x_A_pool2, x_B_pool2), 1)  # 4*4*448(256+192)
        x_global_conv = self.global_conv(x_global_cat)  # 2*2*896

        return x_global_conv

    def forward(self, input_data):
        x = self.features(input_data)
        x = self.avg_pool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


class ParallelCrossCNN(nn.Module):
    """
    Parallel Cross Convolutional Neural Network as described in [2] and found on [4]
    """

    def __init__(self, num_classes=10, head_payload=False):
        super(ParallelCrossCNN, self).__init__()
        if head_payload:
            self.avg_kernel_size = 3
            self.i_size = 22
        else:
            self.avg_kernel_size = 2
            self.i_size = 16
        self.num_class = num_classes
        self.input_size = None
        self.input_size = (self.i_size, self.i_size, 1)
        self.p = 1 if self.i_size == 22 else 0

        self.layer_A_conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*16
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool1 = self.layer_A_pool1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 8*8*32
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_conv2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*64
            nn.BatchNorm2d(64, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_A_pool2 = nn.Sequential(
            nn.Conv2d(64, 96, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 4*4*256
            nn.BatchNorm2d(96, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.layer_B_conv1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 16*16*32
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool1 = nn.MaxPool2d(kernel_size=2, stride=2, padding=0, )  # 8*8*32
        self.layer_B_conv2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=3, stride=1, dilation=1, padding=1, bias=True),  # 8*8*64
            nn.BatchNorm2d(64, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )
        self.layer_B_pool2 = nn.MaxPool2d(kernel_size=2, stride=2, padding=self.p, )  # 4*4*128

        self.global_conv = nn.Sequential(
            nn.Conv2d(128, 160, kernel_size=3, stride=2, dilation=1, padding=1, bias=True),  # 2*2*160
            nn.BatchNorm2d(160, eps=1e-05, momentum=0.9, affine=True),
            nn.ReLU(),
        )

        self.point_conv1 = nn.Conv2d(64, 32, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)
        self.point_conv2 = nn.Conv2d(128, 64, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)
        self.point_conv3 = nn.Conv2d(160, 128, kernel_size=1, stride=1, dilation=1, padding=0, bias=True)

        self.avg_pool = nn.AvgPool2d(kernel_size=self.avg_kernel_size, stride=2, ceil_mode=False)  # 1*1*160

        self.fc = nn.Sequential(
            nn.BatchNorm1d(1 * 1 * 160),
            nn.Dropout(0.5),
            nn.Linear(1 * 1 * 160, self.num_class, bias=True)
        )

    def features(self, input_data):

        x_A_conv1 = self.layer_A_conv1(input_data)  # 16*16*16
        x_B_conv1 = self.layer_B_conv1(input_data)  # 16*16*32
        x_A_pool1 = self.layer_A_pool1(x_A_conv1)  # 16*16*32
        x_B_pool1 = self.layer_B_pool1(x_B_conv1)  # 8*8*32

        x_A_cat1 = torch.cat((x_A_pool1, x_B_pool1), 1)  # 8*8*64
        x_B_cat1 = torch.cat((x_B_pool1, x_A_pool1), 1)  # 8*8*64

        x_A_point_conv1 = self.point_conv1(x_A_cat1)  # 8*8*32
        x_B_point_conv1 = self.point_conv1(x_B_cat1)  # 8*8*32

        x_A_conv2 = self.layer_A_conv2(x_A_point_conv1)  # 8*8*64
        x_B_conv2 = self.layer_B_conv2(x_B_point_conv1)  # 8*8*64

        x_A_cat2 = torch.cat((x_A_conv2, x_B_conv2), 1)  # 8*8*128
        x_B_cat2 = torch.cat((x_B_conv2, x_A_conv2), 1)  # 8*8*128

        x_A_point_conv2 = self.point_conv2(x_A_cat2)  # 8*8*64
        x_B_point_conv2 = self.point_conv2(x_B_cat2)  # 8*8*64


        x_A_pool2 = self.layer_A_pool2(x_A_point_conv2)  # 4*4*96
        x_B_pool2 = self.layer_B_pool2(x_B_point_conv2)  # 4*4*64

        x_global_cat = torch.cat((x_A_pool2, x_B_pool2), 1)  # 4*4*160(96+64)
        x_global_cat = self.point_conv3(x_global_cat)  # 4*4*128
        x_global_conv = self.global_conv(x_global_cat)  # 2*2*160

        return x_global_conv

    def forward(self, input_data):
        x = self.features(input_data)
        x = self.avg_pool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


# TCN implementation as provided by an in [5]:
# Code can be found on their github: https://github.com/locuslab/TCN [6]
class Chomp1d(nn.Module):
    def __init__(self, chomp_size):
        super(Chomp1d, self).__init__()
        self.chomp_size = chomp_size

    def forward(self, x):
        return x[:, :, :-self.chomp_size].contiguous()


class TemporalBlock(nn.Module):
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation, padding, dropout=0.2):
        super(TemporalBlock, self).__init__()
        self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp1 = Chomp1d(padding)
        self.relu1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)

        self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp2 = Chomp1d(padding)
        self.relu2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)

        self.net = nn.Sequential(self.conv1, self.chomp1, self.relu1, self.dropout1,
                                 self.conv2, self.chomp2, self.relu2, self.dropout2)
        self.downsample = nn.Conv1d(n_inputs, n_outputs, 1) if n_inputs != n_outputs else None
        self.relu = nn.ReLU()
        self.init_weights()

    def init_weights(self):
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample is not None:
            self.downsample.weight.data.normal_(0, 0.01)

    def forward(self, x):
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        return self.relu(out + res)


class TemporalConvNet(nn.Module):
    """
    Implementation of the Temporal Convolutional Neural Network as specified and provided in [5] and [6].
    """
    def __init__(self, num_inputs, num_channels, kernel_size=2, dropout=0.2):
        super(TemporalConvNet, self).__init__()
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i-1]
            out_channels = num_channels[i]
            layers += [TemporalBlock(in_channels, out_channels, kernel_size, stride=1, dilation=dilation_size,
                                     padding=(kernel_size-1) * dilation_size, dropout=dropout)]

        self.network = nn.Sequential(*layers)

    def forward(self, x):
        return self.network(x)


class TCN(nn.Module):
    def __init__(self, input_size, output_size, num_channels, kernel_size, dropout=0.2):
        super(TCN, self).__init__()
        self.tcn = TemporalConvNet(input_size, num_channels, kernel_size=kernel_size, dropout=dropout)
        self.linear = nn.Linear(num_channels[-1], output_size)

    def forward(self, inputs):
        """Inputs have to have dimension (N, C_in, L_in)"""
        y1 = self.tcn(inputs)  # input should have dimension (N, C, L)
        o = self.linear(y1[:, :, -1])
        return F.log_softmax(o, dim=1)


if __name__ == "__main__":

    # Randomly generate:
    # batch size = 2
    # packets = 6
    # OHE = 256
    # packet bytes = 100
    # xx = torch.randn(2, 6, 256, 100)
    #
    # convnn = HAST2IDS()
    # print(convnn)
    # yy = convnn(xx)
    # print(xx.size())
    # print(yy.size())
    # print(yy)
    # x = DenoisingTiedAutoEncoder(input_size=1000, hidden_size=1)
    # print(torch.sum(x.forward(torch.ones(1000), 0.1)))
    # print(test, torch.round(test + 0.5))
    # print(torch.round(torch.tensor(0.51)))

    x = torch.randn(2, 1, 22, 22)
    # x = torch.randn(2, 1, 16, 16)

    #print(x.shape)
    #x = x.view(x.shape[0], x.shape[1], -1)
    #print(x.shape)
    #netw = BaseCNN1D(10, 16*16)

    print(x.shape)
    netw = BaseCNN2D(10, True)

    #netw = ParallelCrossCNN(8, False)

    y = netw(x)
    print(y.shape)



    #y = pccn(x)
    #print(y, y.shape)
    pass
