import numpy as np
import json
import os

from .dataset_interface import DatasetInterface
from .enums import FeatureStrategy


class FlowDensityCounter:

    usages = ['train', 'valid', 'test']
    __counter = {usage: {} for usage in usages}

    def __init__(self,
                 interface=None,
                 verbose=True,
                 path=None,
                 load=False):
        self.verbose = verbose

        if load:
            self.__load(path)
        else:
            self.__initialize(interface=interface)

    def __initialize(self, interface: DatasetInterface):
        """
        Initialize the FlowDensityCounter by actually counting the flow densities.

        """
        # Counting structure: For m labels and a flow depth of n packets:
        # Usage can be any of 'train', 'valid' or 'test'
        # {
        #   usage:
        #   {
        #       Label1: {'1': count1, '2': count2, ..., 'n': countn},
        #       Label2: {'1': count1, '2': count2, ..., 'n': countn},
        #       ...,
        #       Labelm: {'1': count1, '2': count2, ..., 'n': countn},
        #   }
        #
        # }
        self.interface = interface
        self.labels = self.interface.get_labels(only_labelled_days=True)
        _, self.n_stream_packets = self.interface.get_stream_params()

        for usage in self.usages:
            for label in self.labels:
                self.__counter[usage][label] = {str(i): 0 for i in range(1, self.n_stream_packets + 1)}

        self.__count()

    def __count(self):
        """
        Function to count the packets in flows in the memory mapped data.

        """
        memmap_metadata = self.interface.get_memmap_metadata()
        packet_size = self.__get_packet_size()

        total_usages = len(self.usages)
        total_labels = len(self.labels)

        for usage_i in range(total_usages):
            usage = self.usages[usage_i]
            for label_i in range(total_labels):
                label = self.labels[label_i]
                [memmap_path, memmap_shape] = memmap_metadata[usage][label]

                memmap = np.memmap(filename=memmap_path, dtype=self.interface.get_dtype(),
                                   mode='r', shape=tuple(memmap_shape))

                for i in range(memmap_shape[0]):
                    if self.verbose:
                        print('Progress: {}: {}/{}-{}/{}-{}/{}                      '.format(
                            usage, usage_i+1, total_usages, label_i+1, total_labels, i+1, memmap_shape[0]), end='\r')

                    n = self.__n_packets_in_flow(memmap[i], packet_size)
                    self.__counter[usage][label][str(n)] += 1
        print()

    def __n_packets_in_flow(self, sample, packet_size):
        if self.interface.get_feature_strategy() == FeatureStrategy.PCCN:
            return self.__n_pccn_packets(sample, packet_size)
        else:
            raise NotImplementedError

    def __n_pccn_packets(self, sample, packet_size):
        n = 0   # Number of packets
        i = 0   # Position of byte pointer
        sample = sample.flatten()  # Go from n x n -> n² x 1

        # For non-Header-Payload features, skip the first FF byte
        if not self.interface.get_feature_type() == 'hepa':
            i += 1

        for _ in range(self.n_stream_packets):
            packet = sample[i:i+packet_size]

            if packet.sum() == 0:
                break

            n += 1
            i += packet_size + 1

        return n

    def __get_packet_size(self):

        sample_shape = self.interface.get_shape()

        if self.interface.get_feature_strategy() == FeatureStrategy.PCCN:
            n_ff = self.n_stream_packets - 1
            if not self.interface.get_feature_type() == 'hepa':
                n_ff += 2

            return int((np.product(sample_shape) - n_ff) / self.n_stream_packets)

    def get_number_samples(self):
        total = 0
        overall_count = self.get_overall_count()
        for _, count in overall_count.items():
            total += count

        return total


    def get_overall_count(self):

        counts = [str(i) for i in range(1, self.n_stream_packets + 1)]
        overall = {count: 0 for count in counts}

        for usage in self.usages:
            for label in self.labels:
                for count in counts:
                    overall[count] += self.__counter[usage][label][count]

        return overall

    def get_overall_percentages(self):
        overall_count = self.get_overall_count()

        total_count = 0

        for _, item in overall_count.items():
            total_count += item

        overall_percentages = overall_count

        for key, item in overall_count.items():
            overall_percentages[key] = np.round(item / total_count * 100, 2)

        return overall_percentages

    def get_overview_per_class(self):

        counts = [str(i) for i in range(1, self.n_stream_packets + 1)]
        overview_per_class = {label: {count: 0 for count in counts} for label in self.labels}

        for usage in self.__counter.keys():
            for label in self.labels:
                for count in counts:
                    overview_per_class[label][count] += self.__counter[usage][label][count]
        return overview_per_class

    def get_percentage_per_class(self):

        overview_per_class = self.get_overview_per_class()
        percentage_per_class = overview_per_class

        for label, overview in overview_per_class.items():
            total_count = 0

            for size, count in overview.items():
                total_count += count

            for size, count in percentage_per_class[label].items():
                percentage_per_class[label][size] = np.round(count / total_count * 100, 2)

        return percentage_per_class

    def store(self, path, name):

        if len(name) > 5:
            if not (name[-5:] == '.json'):
                name += '.json'
        else:
            name += '.json'

        storage_dict = {
            'interface_path': self.interface.root_directory,
            'counter': self.__counter
        }

        with open(os.path.join(path, name), 'w') as f:
            json.dump(storage_dict, f)

    def __load(self, path):
        with open(path, 'r') as f:
            storage_dict = json.load(f)

        self.interface = DatasetInterface(storage_dict['interface_path'])
        self.labels = self.interface.get_labels(only_labelled_days=True)
        _, self.n_stream_packets = self.interface.get_stream_params()
        self.__counter = storage_dict['counter']


def count_flow_density(interface):
    memmap_metadata = interface.get_memmap_metadata()




