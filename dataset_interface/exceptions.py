class PreprocessorException(Exception):
    """
    Basic exception raised for anything that has to do with the preprocessor.
    """
    pass


class MemoryOverloadException(PreprocessorException):
    """
    Exception thrown when too much memory is consumed 
    """
    pass

class CTypesInvalidArgument(PreprocessorException):
    """
    Thrown when an invalid argument is given in a function that call a C library function.
    """
    pass

class CInternalError(PreprocessorException):
    """
    Thrown if a C function experiences an error internally.
    """
    pass

class DatasetPreprocessError(PreprocessorException):
    """
    Basic exception corresponding to dataset preprocessing exceptions
    """
    pass

class DatasetPreprocessInvalidArgument(DatasetPreprocessError):
    """
    Thrown when an invalid argument is given as input for a dataset preprocessing function
    """
    pass

class MLConfigurationConstructionException(PreprocessorException):
    """
    Thrown when the construction of the configuration of a machine learning setup is obstructed.
    """
    pass

class DatasetConfigurationConstructionException(PreprocessorException):
    """
    Thrown when the construction of the configuration of a dataset is obstructed
    """
    pass

class FeatureStrategyConfigurationConstructionException(PreprocessorException):
    """
    Thrown when the construction of the configuration of a feature strategy is obstructed
    """
    pass

class MachineLearningException(Exception):
    """
    Basic exception raised for anything that has to do with machine learning
    """
    pass

class DatasetSplitException(MachineLearningException):
    """
    Thrown for exceptions during the splitting of the dataset
    """
    pass

class NotImplementedException(Exception):
    """
    Thrown when certain functionality has not been implemented yet
    """
    pass

class MissingTimestampsException(Exception):
    """
    Raised when no timestamps are provided but are necessary.
    """
    pass

class InvalidCorruptionValue(Exception):
    """
    Raised when an invalid corruption value is passed.
    """
    pass