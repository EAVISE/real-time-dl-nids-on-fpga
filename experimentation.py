from dataset_interface.dataset_interface import DatasetInterface

from experiment_metadata.config import configuration_exists
from experiment_metadata.experiment_config import SupervisedMLExperiment

from evaluation.evaluation import evaluation_from_cm_dict

from learning.training import train_experiment
from learning.testing import test_experiment
from learning.inference import supervised_inference, unsupervised_inference, sanity_check

import os
import numpy as np


def add_training_epochs(name, root, n_epochs, verbose=True):
    experiment = __get_experiment(name, root, use_existing=True, supervised=True)
    experiment.add_training_epochs(n_epochs)

    __conduct_experiment(experiment, verbose=verbose)


def experimentation(name, root, use_existing=True, supervised=True, verbose=True, **kwargs):
    # First, get the experiment
    experiment = __get_experiment(name, root, use_existing=use_existing, supervised=supervised, **kwargs)
    # Then, conduct that experiment
    __conduct_experiment(experiment, verbose=verbose)


def get_existing_experiment(name, root):
    return __get_experiment(name, root, use_existing=True)


def __get_experiment(name, root, use_existing=True, supervised=True, **kwargs):
    
    print('Use existing: {}'.format(use_existing))
    print('Config exists: {}, for {}'.format(configuration_exists(name, root), os.path.join(root, 'config', name + '.json')))
    if use_existing and configuration_exists(name, root):
        if supervised:
            return SupervisedMLExperiment(root, name)
    else:
        return __construct_experiment(name, root, supervised, **kwargs)


def __construct_experiment(name, root, supervised=True, **kwargs):
    if supervised:
        interface = DatasetInterface(kwargs['dataset_dir'])
        return SupervisedMLExperiment(root, name, dataset_interface=interface, **kwargs)
    else:
        return None  # Under construction


def __conduct_experiment(experiment, verbose=True):
    """
    Conduct an experiment. Depending on the training status, this means that either a model will be trained and
    validated, or that a model will be tested.
    :param experiment:
    :param verbose:
    :return:
    """
    if verbose:
        print("Conducting experiment \"", experiment.name, "\"")

    if not experiment.status['train']['complete']:
        # Train the model
        train_experiment(experiment, verbose=verbose)
    elif not experiment.status['test']['complete']:
        # Test the model
        test_experiment(experiment, verbose=verbose)
    else:
        evaluation = evaluation_from_cm_dict(experiment.get_results_cm(), False)
        print(evaluation.classification_report())
        print(evaluation.evaluate_ids(experiment.dataset_interface.get_normal_class()))


def conduct_sanity_check(experiment, output_path):
    """
    For a provided experiment, conduct inference with one randomly generated input sample, and get the output sample.
    Both input and output will be written to NPY files.

    :param experiment:
    :param output_path:
    :return:
    """

    random_sample = np.random.rand(*experiment.dataset_interface.get_shape()).astype(np.float32)
    random_sample = np.expand_dims(np.expand_dims(random_sample, axis=0), axis=0)

    input_npy, output_npy = sanity_check(experiment, random_sample, verbose=True)

    base_output_path = output_path.rstrip('.npy')
    print("Storing input  to {}".format(base_output_path + '-input.npy'))
    print("Storing output to {}".format(base_output_path + '-output.npy'))

    np.save(base_output_path + '-input.npy', input_npy)
    np.save(base_output_path + '-output.npy', output_npy)


def conduct_inference(experiment, data_file, evaluation_file, file_type, output_path, supervised=True):
    """
    Conduct inference using the model trained in the specified experiment, for the provided input data

    :param experiment:
    :param data_file:
    :param evaluation_file: Expected outputs for the provided inputs. Optional.
    :param file_type:
    :param supervised: If True, the inference will consider the expected outputs for the provided inputs.
    :return:
    """
    if supervised:
        __supervised_inference(experiment, data_file, evaluation_file, file_type, output_path)
    else:
        __unsupervised_inference(experiment, data_file, file_type, output_path)


def __supervised_inference(experiment, data_file, evaluation_file, file_type, output_path):
    data_arrays = __load_inference_file_as_numpy(data_file, file_type)
    eval_arrays = __load_inference_file_as_numpy(evaluation_file, file_type)

    supervised_inference(experiment, data_arrays, eval_arrays, output_path, True)


def __unsupervised_inference(experiment, data_file, file_type, output_path):
    data_arrays = __load_inference_file_as_numpy(data_file, file_type)

    unsupervised_inference(experiment, data_arrays, output_path, True)


def __load_inference_file_as_numpy(file_path, file_type):
    """

    :param file_path: Path to file with inference data
    :param file_type: File type, must be: npy
    :type file_path: ``str``
    :type file_type: ``str``

    :return: A Numpy array representing the data of the input file.
    """
    if file_type == 'npy':
        return np.load(file_path, 'r').copy()
    else:
        return None
