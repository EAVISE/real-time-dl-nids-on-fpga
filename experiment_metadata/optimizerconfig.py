from experiment_metadata.enums import Optimizer

class OptimizerConfig:

    def __init__(self, name=None, from_dict=None, **kwargs):
        if not (from_dict is None):
            self._from_dict(from_dict, **kwargs)
        else:
            self._from_kwargs(name, **kwargs)

    def _from_dict(self, dictionary):
        self.name = Optimizer[dictionary['name']]

    def _from_kwargs(self, name, **kwargs):
        self.name = name

    def to_dict(self, **kwargs):
        return {'name': self.name.name}

    def get_parameters(self):
        return {}


def _optimizer_to_config(optim):
    """
    For a given optimizer, get the configuration class.

    :param optim: Specifief optimizer.
    :type optim: ``Optimizer.RMSprop``, ``Optimizer.SGD``
    :return: subclass of OptimizerConfig
    :rtype: ``RMSpropConfig``, ``SGDConfig``
    """
    if optim == Optimizer.RMSprop:
        return RMSpropConfig
    elif optim == Optimizer.SGD:
        return SGDConfig
    return None


def get_optimizer(name=None, from_dict=None, **kwargs):
    if from_dict:
        name = Optimizer[from_dict['name']]

    return _optimizer_to_config(name)(from_dict=from_dict, **kwargs)


class RMSpropConfig(OptimizerConfig):

    def __init__(self, from_dict=None, **kwargs):
        super().__init__(name=Optimizer.RMSprop, from_dict=from_dict, **kwargs)

    def _from_kwargs(self, name, **kwargs):
        super()._from_kwargs(name)
        self.eps = kwargs.get('eps', 1e-07)
        self.alpha = kwargs.get('alpha', 0.9)
        self.weight_decay = kwargs.get('weight_decay', 0)
        self.momentum = kwargs.get('momentum', 0)
        self.centered = kwargs.get('centered', False)

    def _from_dict(self, dictionary):
        super()._from_dict(dictionary)
        self.eps = dictionary['eps']
        self.alpha = dictionary['alpha']
        self.weight_decay = dictionary['weight_decay']
        self.momentum = dictionary['momentum']
        self.centered = dictionary['centered']

    def to_dict(self, **kwargs):
        dictionary = super(RMSpropConfig, self).to_dict()
        dictionary['eps'] = self.eps
        dictionary['alpha'] = self.alpha
        dictionary['weight_decay'] = self.weight_decay
        dictionary['momentum'] = self.momentum
        dictionary['centered'] = self.centered

        return dictionary

    def get_parameters(self):
        return {
            'eps': self.eps,
            'alpha': self.alpha,
            'weight_decay': self.weight_decay,
            'momentum': self.momentum,
            'centered': self.centered
        }


class SGDConfig(OptimizerConfig):

    def __init__(self, from_dict=None, **kwargs):
        super().__init__(name=Optimizer.SGD, from_dict=from_dict, **kwargs)

    def _from_kwargs(self, name, **kwargs):
        super()._from_kwargs(name)
        self.weight_decay = kwargs.get('weight_decay', 0)
        self.momentum = kwargs.get('momentum', 0.9)
        self.dampening = kwargs.get('dampening', 0)
        self.nesterov = kwargs.get('nesterov', False)

    def _from_dict(self, dictionary):
        super()._from_dict(dictionary)
        self.weight_decay = dictionary['weight_decay']
        self.momentum = dictionary['momentum']
        self.dampening = dictionary['dampening']
        self.nesterov = dictionary['nesterov']

    def to_dict(self, **kwargs):
        dictionary = super(SGDConfig, self).to_dict()
        dictionary['nesterov'] = self.nesterov
        dictionary['weight_decay'] = self.weight_decay
        dictionary['momentum'] = self.momentum
        dictionary['dampening'] = self.dampening

        return dictionary

    def get_parameters(self):
        return {
            'nesterov': self.nesterov,
            'dampening': self.dampening,
            'weight_decay': self.weight_decay,
            'momentum': self.momentum,
        }
