from enum import Enum


class Optimizer(Enum):
    RMSprop = 1
    SGD = 2

class Model(Enum):
    PCCN = 1
    HAST_I = 2
    HAST_II = 3
    ResNet50 = 4
    GoogLeNet = 5
    SimpleNet = 6
    TiedSDA = 7
    TCN = 8
    PCCNN = 9
    QPPCCN = 10
    SimpleQPPCCN = 11
    BaseCNN1D = 12
    BaseCNN2D = 14
    QBaseCNN2DAvgPool_W2A2 = 15
    QBaseCNN2DAvgPool_W4A4 = 16
    QBaseCNN2DAvgPool_W8A8 = 17
    QBaseCNN2DAvgPool_W16A16 = 17
    BaseCNN2D2 = 18
    #QBaseCNN1D = 13

    #QBaseCNN2D = 15
    #QTestCNN2D = 16
    #CNV_W2A2 = 17
    #CNV_W1A1 = 18
    #QBaseCNN2D_W1A1 = 19
    #QBaseCNN2D_W2A2 = 20
    #QBaseCNN2dNP_W1A1 = 21
    #QBaseCNN2dNP_W2A2 = 22
    #QBaseCNN2dNP_W4A4 = 23
    #BaseCNN2dNP = 24
    #QBaseCNN2dAP = 25
    #QBaseCNN2dMP1024_W2A2 = 26
    #TestDependencies = 27

def is_supervised(model):
    if model == Model.TiedSDA:
        return False
    else:
        return True
